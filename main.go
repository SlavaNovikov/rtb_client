package main

import (
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"sync/atomic"
	"time"
)

var (
	codes   = make(map[int]*int64, 1000)
	limit   *int
	counter int64
	wg      sync.WaitGroup
)

func main() {

	type D map[string]string

	timeout := flag.Int("t", 1000, "timeout between requests (millisecond)")
	port := flag.Int("p", 4000, "port where run rtb server")
	limit = flag.Int("n", -1, "count requests, unlimit by default")
	pid := flag.String("pid", "1_site1_sub", "fixed pid")
	flag.Parse()

	var i int
	if *limit > 0 {
		go func() {
			wg.Add(*limit)
			wg.Wait()
		}()
		i = 0
	} else {
		i = -1
	}
	for i <= *limit {
		if *limit > 0 {
			i++
		}
		if *timeout > 0 {
			time.Sleep(time.Duration(int64(time.Millisecond) * int64(*timeout)))
		}
		go func() {
			defer func() {
				atomic.AddInt64(&counter, 1)
				if *limit > 0 {
					wg.Done()
				}
			}()
			var d = D{
				"ua":      "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36",
				"referer": "http://zoosfera-nn.ru/Default.aspx?asdf",
				"link":    fmt.Sprintf("http://127.0.0.1:%d/cl/?guid=testGuid&prid=132&pid=%s%d", *port, *pid, rand.Intn(1000)),
			}
			client := http.Client{}
			req, err := http.NewRequest("GET", d["link"], nil)
			if err != nil {
				fmt.Println(err)
				return
			}
			req.Header.Set("User-Agent", d["ua"])
			req.Header.Set("Referer", d["referer"])

			res, err := client.Do(req)
			if err != nil {
				fmt.Println(err)
				return
			}
			defer func() {
				if err := res.Body.Close(); err != nil {
					fmt.Println(err)
				}
			}()
			if codes[res.StatusCode] != nil {
				atomic.AddInt64(codes[res.StatusCode], 1)
			} else {
				v := int64(1)
				codes[res.StatusCode] = &v
			}
			if counter != 0 && counter%1000 == 0 {
				fmt.Println("iter.........")
				printResult()
			}
			return
		}()
	}
	printResult()
}

func printResult() {
	fmt.Println("requests: ", counter)
	fmt.Print("codes:")
	for code := range codes {
		fmt.Printf("\t%v:%v", code, *codes[code])
	}
	fmt.Println("\n----")
}
