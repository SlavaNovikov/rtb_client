# coding: utf-8
import datetime

from fabric.api import env, local, put, task, run, sudo

env.hosts = ['localhost']
env.user = 'ubuntu'

@task
def deploy():
    """ деплой приложения """
    print(datetime.datetime.now())

    local("env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ./bin/client -a -installsuffix cgo -ldflags '-s'")

    put('bin/client', '/tmp/client', mode=0755)
    sudo("service client stop")
    run('mv /tmp/client /opt/rtb/client')
    sudo("service client start")